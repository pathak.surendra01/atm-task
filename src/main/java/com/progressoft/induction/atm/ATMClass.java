/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progressoft.induction.atm;

import com.progressoft.induction.atm.exceptions.AccountNotFoundException;
import com.progressoft.induction.atm.exceptions.InsufficientFundsException;
import com.progressoft.induction.atm.exceptions.NotEnoughMoneyInATMException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author User
 */
public class ATMClass implements ATM{
    BigDecimal accountBalance; 
    private BankingSystemClass a;
    
    public ATMClass() {
        this.a = new BankingSystemClass();
    }
    
    // Main 
    public static void main(String[] args) {    
        ATMClass ATMMachine = new ATMClass();
        List<Banknote> transactionWithdraw = ATMMachine.withdraw("111111111",new BigDecimal(500));
    }
    
    // Denomination and count money
    public List<Object> withdrawAmount(BigDecimal amount) {
        BigDecimal sum = new BigDecimal(0);
        List<Object> withdrawBanknoteList = new ArrayList<Object>();
        int[] notesAvailable = new int[]{10,20,100,100};
        int[] notesCount = new int[4];
        int[] notes = new int[]{50,20,10,5};
        int amountCopy = Integer.valueOf(amount.intValue());
        // Count proper money notes
        for(int i=0;i<4;i++) {
            int count = amountCopy/notes[i];
            if (count>notesAvailable[i]){
                count = notesAvailable[i];
                notesCount[i]=count;
                amountCopy = amountCopy - count*notes[i];
                continue;
            }
            notesCount[i]=count;
            amountCopy = amountCopy % notes[i];
        }
        // Print denomination
        for(int i=0;i<4;i++){
            if(notesCount[i]!=0) {
                System.out.println(notes[i]+"\tx\t"+notesCount[i]+"\t= "+notes[i]*notesCount[i]);
            }
        }
        System.out.println("--------------------------------");
        System.out.println("TOTAL\t\t\t= "+amount); 
        return withdrawBanknoteList;
    }
    
    @Override
    public List<Banknote> withdraw(String accountNumber, BigDecimal amount) {
        List<Banknote> banknoteList = new ArrayList<Banknote>();
        // Check atm balance/account balance/account number and handle exception
        try {
            accountBalance = a.getAccountBalance(accountNumber);
            if (amount.compareTo(a.atmBalance()) > 0) {
                throw new NotEnoughMoneyInATMException();
            } else {
                if (amount.compareTo(accountBalance) <= 0) {
                    // debit account and call method for withdraw/denomination
                    a.debitAccount(accountNumber, amount);
                    withdrawAmount(amount);
                } else {
                    throw new InsufficientFundsException();
                }
            }
        } catch (AccountNotFoundException e) {
            System.out.println(e.AccountException());
        }
        
        try {
            
        } catch (InsufficientFundsException e) {
            e.FundsException();
        }
        return banknoteList;
    }
}