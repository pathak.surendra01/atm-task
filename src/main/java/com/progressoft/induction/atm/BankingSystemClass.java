/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progressoft.induction.atm;

import com.progressoft.induction.atm.exceptions.AccountNotFoundException;
import java.math.*;

/**
 *
 * @author User
 */
public class BankingSystemClass implements BankingSystem {
    double accountBalance;
    String accountNumber;
    int atmBalance = 2400;
    boolean accountFoundFlag = false;
    boolean transactionErrorFlag = true;    
    String[] accountList = {"123456789", "111111111", "222222222", "333333333", "444444444"};
    double[] accountBalanceList = {1000.0, 1001.0, 1002.0, 1003.0, 1004.0};
    
    
    // Gets account balance
    // handles exception if invalid account number ie. AccountNotFoundException
    @Override
    public BigDecimal getAccountBalance(String accountNumber) throws AccountNotFoundException {        
        this.accountNumber = accountNumber;
        for (int i = 0; i < accountList.length; i++) {
            if (accountList[i].equals(this.accountNumber)) {
                this.accountBalance = accountBalanceList[i];
                this.accountFoundFlag = true;
            }
        }
        if(!this.accountFoundFlag) {
            throw new AccountNotFoundException(this.accountNumber);
        }        
        BigDecimal balance = new BigDecimal(this.accountBalance);
        return balance;
    }   
    
    // debit account before withdrawal
    @Override
    public void debitAccount(String accountNumber, BigDecimal amount) {
       this.accountNumber = accountNumber;
        for (int i = 0; i < accountList.length; i++) {
            if (accountList[i].equals(this.accountNumber)) {
                double amountDouble = amount.doubleValue();
                accountBalanceList[i] -= amountDouble;
                System.out.println("Your new balance is:"+accountBalanceList[i]);
                this.transactionErrorFlag = false;
                return;
            }
        }
        if(!this.transactionErrorFlag) {
            System.out.println("Transscation error");
        } 
    }
    
    
    // Calculate ATM balance/JOD notes
    public BigDecimal atmBalance() {
        BigDecimal sum = new BigDecimal(0);
        Banknote[] note = Banknote.values();
        for(Banknote item:note) {
            if(item.getValue().compareTo(new BigDecimal("5.0"))==0){
                sum = sum.add(item.getValue().multiply(new BigDecimal(100)));
            } else if (item.getValue().compareTo(new BigDecimal("10.0"))==0) {
                sum = sum.add(item.getValue().multiply(new BigDecimal(100)));
            } else if (item.getValue().compareTo(new BigDecimal("20.0"))==0) {
                sum = sum.add(item.getValue().multiply(new BigDecimal(20)));
            } else if (item.getValue().compareTo(new BigDecimal("50.0"))==0) {
                sum = sum.add(item.getValue().multiply(new BigDecimal(10)));
            } 
        }
        return sum;
    }
    
}
