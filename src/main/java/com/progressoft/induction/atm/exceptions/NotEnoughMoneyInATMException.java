package com.progressoft.induction.atm.exceptions;

public class NotEnoughMoneyInATMException extends RuntimeException {
     public void NotEnoughMoneyInATM(){
        System.out.println("Not enough balance in the atm");
    }
}
