package com.progressoft.induction.atm.exceptions;

public class AccountNotFoundException extends RuntimeException {
    String accountNumber;
    
    public AccountNotFoundException(String accountNumber) {
        this.accountNumber = accountNumber;
    }
    
    public String AccountException(){
        return(accountNumber + " is not a valid account number");
    }
}
